﻿using System;

namespace Fibonacci
{
   
    class Program
    { 
        public static int final = 0;
        static void Main(string[] args)
        {
            int prevIt = 1;
            int iterator = 1;
            int count = 1;
            while(iterator < 4000000)
            {
                int oldIt = iterator;
                if (iterator % 2 == 0)
                {
                    final = final + iterator;
                }
                iterator = iterator + prevIt;
                prevIt = oldIt;
                Console.WriteLine("Fibonacci number " + count + " is: " + oldIt);
                count++;
            }
            //Console.WriteLine("Fibonacci number " + count + " is: " + iterator);
            Console.WriteLine("Sum of even numbered fibonacci numbers: " + final);
        }
    }
}
